@extends('layouts.app')

@section('header', 'Edit product')

@section('content')
    <div class="container">
        <form class="form" id="edit" method="post" action="{{ route('products.update', ['product' => $product]) }}">
            @csrf
            @method('PATCH')
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control" name="name" type="text" placeholder="Enter product name" value="{{ $product->name }}" required>
            </div>

            <div class="form-group">
                <label for="price">Price</label>
                <input class="form-control" name="price" type="number" placeholder="Enter product price" value="{{ $product->price }}" required>
            </div>

            <div class="text-center">
                <button class="btn btn-lg btn-block btn-primary" form="edit" type="submit">Save</button>
                <button type="submit" form="delete" class="btn btn-sm btn-danger">Delete</button>
            </div>

        </form>

        <form class="form" id="delete" method="post" action="{{ route('products.destroy', ['product' => $product]) }}">
            @csrf
            @method('DELETE')
        </form>

        <div class="invalid-feedback">
            @foreach($errors->all() as $error)
                <span class="text-danger">{{$error}}</span>
            @endforeach
        </div>

    </div>
@endsection