@extends('layouts.app')

@section('header', 'Add new product')

@section('content')
    <div class="container">
    <form class="form" id="add" method="post" action="{{ route('products.store') }}">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input class="form-control" name="name" type="text" placeholder="Name" required>
        </div>

        <div class="form-group">
            <label for="price">Price</label>
            <input class="form-control" name="price" type="number" placeholder="Price" required>
        </div>
        <div class="text-center">
            <button class="btn btn-lg btn-block btn-primary" type="submit">Save</button>
        </div>
    </form>

    <div class="invalid-feedback">
        @foreach($errors->all() as $error)
            <span class="text-danger">{{$error}}</span>
        @endforeach
    </div>
    </div>

@endsection