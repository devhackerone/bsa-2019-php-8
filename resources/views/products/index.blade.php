@extends('layouts.app')

@section('header', 'Products')

@section('content')
    <div class="col-md-4 h2 text-md-left">
        <a class="btn btn-lg btn-success " href="{{ route('products.create') }}" role="button">Add</a>
    </div>
    @if ($products->isNotEmpty())
    <div class="container">
        @if(Session::has('error'))
            <span class="text-danger"> {{ Session::get('error') }} </span>
        @endif
        <ul class="list-group">
            @foreach($products as $product)
                <li class="list-group-item">
                    <a href="{{ route('products.show', ['product' => $product]) }}">{{ $product->name }}</a>
                    <p>{{ $product->price }}</p>

                @can('update', $product)
                    <p class="list-group-item">
                        <a class="btn btn-sm btn-primary" href="{{ route('products.edit', ['product' => $product]) }}" role="button">Edit</a>
                    </p>
                @endcan

                @can('delete', $product)
                    <span class="list-group-item">
                        <form method="post" action="{{ route('products.destroy', ['product' => $product]) }}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-sm btn-danger" title="Delete" type="submit">Delete</button>
                        </form>
                    </span>
                @endcan
                </li>
            @endforeach
        </ul>

        <div class="invalid-feedback">
            @foreach($errors->all() as $error)
                <span class="text-danger">{{$error}}</span>
            @endforeach
        </div>

    </div>

    @else
        <div class="text-center h1">
            No products to display!
        </div>
    @endif

@endsection