@extends('layouts.app')

@section('header')
    <div class="text-center h1">
        <a href="{{ route('products.index') }}">
            Back to all products</a>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="h2 text-md-left">
            Product № {{ $product->id }}
        </div>

        <ul class="list-group">
            @if($product)
                <li class="list-group-item">
                    <p>Name: {{ $product->name }}</p>
                    <p>Price: {{ $product->price }}</p>
                    <p>User: {{ $product->user->name }}</p>
                </li>

                    @can('update', $product)
                        <li class="list-group-item">
                            <a class="btn btn-sm btn-primary" href="{{ route('products.edit', ['product' => $product]) }}" role="button">Edit</a>
                        </li>
                    @endcan

                    @can('delete', $product)
                        <li class="list-group-item">
                            <form method="post" action="{{ route('products.destroy', ['product' => $product]) }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-sm btn-danger" title="Delete" type="submit">Delete</button>
                            </form>
                        </li>
                    @endcan

            @endif
        </ul>

        <div class="invalid-feedback">
            @foreach($errors->all() as $error)
                <span class="text-danger">{{$error}}</span>
            @endforeach
        </div>

    </div>
@endsection