<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entity\Product;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        return view('products.index')
            ->with(compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create', Product::class)) {
            Session::flash('alert-warning', '401 Unauthorized');

            return redirect()->route('products.index');
        }

        return view('products.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('create', Product::class)) {
            Session::flash('alert-warning', '401 Unauthorized');

            return redirect()->route('products.index');
        }

        $data = $request->validate([
            'name' => ['required', 'between:3, 255'],
            'price' => ['required', 'numeric', 'min:0']
        ]);

        $data['user_id'] = auth()->user()->id;
        Product::create($data);

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        if (Gate::denies('view', $product)) {
            Session::flash('alert-warning', '401 Unauthorized');

            return redirect()->route('products.index');
        }

        return view('products.show')->with(compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        if (Gate::denies('update', $product))
        {
            Session::flash('alert-warning', '401 Unauthorized');

            return redirect()->route('products.index');
        }

        return view('products.edit')->with(compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        if (Gate::denies('update', $product))
        {
            Session::flash('alert-warning', '401 Unauthorized');

            return redirect()->route('products.index');
        }

        $data = $request->validate([
            'name' => ['required', 'between:3, 255'],
            'price' => ['required', 'numeric', 'min:0']
        ]);

        $product->update($data);
        $product->save();

        return redirect()->route('products.show', ['product' => $product]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if ($product) {
            if (Gate::denies('delete', $product)) {
                Session::flash('alert-warning', '401 Unauthorized');

                return redirect()->route('products.index');
            }

            Product::destroy($id);
        }

        return redirect()->route('products.index');
    }
}
